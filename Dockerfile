FROM debian:7
MAINTAINER Richard Delaplace <rdelapla@gmail.com>
LABEL Description="Simple Nginx/Php5/Mysql server" \
  Vendor="Yueyehua" \
  Version="1.0.0"

ENV _PHP_VERSION="5.4.45-1~dotdeb+7.1"
ENV _MYSQL_VERSION="5.6.34-1~dotdeb+7.2"
ENV _GITLFS_VERSION="1.1.1"
ENV _NODE_VERSION="7.2.0"

RUN apt-get update && \
  apt-get -y install \
    wget \
    git \
    curl \
    nginx \
    ffmpeg \
    bzip2 \
    cachefilesd \
    memcached \
    vim

ENV _GITLFS_REPO=https://packagecloud.io/install/repositories/github/
ENV _GITLFS_DEB=git-lfs/script.deb.sh
ADD environment.list /etc/apt/sources.list.d/environment.list
RUN wget -qO- ${_GITLFS_REPO}${_GITLFS_DEB} | bash && \
  wget --no-check-certificate -qO - https://www.dotdeb.org/dotdeb.gpg | \
  apt-key add - && \
  wget --no-check-certificate -qO - https://packagecloud.io/gpg.key | \
  apt-key add - && \
  apt-key adv --recv-keys --keyserver hkp://pgp.mit.edu:80 0xA5D32F012649A5A9

RUN apt-get update && \
  apt-get -y install \
    jq \
    redis-server \
    phpmyadmin \
    blackfire-agent \
    blackfire-php \
    nodejs \
    nodejs-legacy \
    php5=${_PHP_VERSION} \
    php5-common=${_PHP_VERSION} \
    php5-dev=${_PHP_VERSION} \
    php5-mysqlnd=${_PHP_VERSION} \
    php5-curl=${_PHP_VERSION} \
    php5-cli=${_PHP_VERSION} \
    php5-fpm=${_PHP_VERSION} \
    php-pear=${_PHP_VERSION} \
    php5-memcached=${_PHP_VERSION} \
    php5-apc=${_PHP_VERSION} \
    php5-mcrypt=${_PHP_VERSION} \
    php5-gd=${_PHP_VERSION} \
    php5-memcache=${_PHP_VERSION} \
    php5-redis=${_PHP_VERSION} \
    php5-imagick=${_PHP_VERSION} \
    php5-xdebug=${_PHP_VERSION} \
    mysql-common=${_MYSQL_VERSION} \
    libmysqlclient18=${_MYSQL_VERSION} \
    mysql-client=${_MYSQL_VERSION} \
    mysql-server=${_MYSQL_VERSION} \
    git-lfs=${_GITLFS_VERSION}

RUN cd /root && \
  pear install \
    http_request2 && \
  wget -qO- https://www.npmjs.org/install.sh | bash && \
  npm install -g gulp && \
  npm install -g n && \
  n ${_NODE_VERSION} && \
  ln -sf /usr/local/n/versions/node/${_NODE_VERSION}/bin/node $(which node)

RUN touch /usr/share/mysql/my-default.cnf && \
  mysql_install_db --user=mysql --basedir=/usr/ --ldata=/var/lib/mysql/ && \
  service mysql start && \
  mysqladmin -u root password vagrant && \
  mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root -pvagrant mysql && \
  service mysql stop

ADD startup.sh /opt/startup.sh
ENTRYPOINT /opt/startup.sh

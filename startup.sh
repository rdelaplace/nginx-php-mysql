#!/bin/bash

service mysql restart
service nginx restart

tail -F /var/log/{nginx,mysql}/*.log
